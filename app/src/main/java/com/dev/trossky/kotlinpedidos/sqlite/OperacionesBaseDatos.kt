package com.dev.trossky.kotlinpedidos.sqlite

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteQueryBuilder
import com.dev.trossky.kotlinpedidos.model.*
import java.time.format.FormatStyle

/**
 * Created by luis on 3/10/17.
 */
class OperacionesBaseDatos {

    private constructor(){}
    companion object {
        private var baseDatos: BaseDatosPedidos? = null
        private val instancia = OperacionesBaseDatos()


        fun obtenerInstancia(contexto: Context): OperacionesBaseDatos {
            if (baseDatos == null) {
                baseDatos = BaseDatosPedidos(contexto)
            }
            return instancia
        }
    }




    /** [ContratoPedidos.CabeceraPedido]
     * OPERACIONES
     */
    //@GET ALL CabeceraPedido
    fun obtenerCabeceraPedido(): Cursor {
        val db = baseDatos!!.readableDatabase
        val builder = SQLiteQueryBuilder()
        builder.tables = CABECERA_PEDIDO_JOIN_CLIENTE_Y_FORMA_PAGO


        return builder.query(db, proyCabeceraPedido, null, null, null, null, null)

    }

    private val CABECERA_PEDIDO_JOIN_CLIENTE_Y_FORMA_PAGO = "cabecera_pedido " +
            "INNER JOIN cliente " +
            "ON cabecera_pedido.id_cliente= cliente.id " +
            "INNER JOIN forma_pago " +
            "ON cabecera_pedido.id_forma_pago=forma_pago.id "

    private val proyCabeceraPedido = arrayOf<String>(BaseDatosPedidos.Tablas.CABECERA_PEDIDO + "." + ContratoPedidos.ColumnasCabeceraPedido.ID, ContratoPedidos.ColumnasCabeceraPedido.FECHA, ContratoPedidos.ColumnasCliente.NOMBRES, ContratoPedidos.ColumnasCliente.APELLLIDOS, ContratoPedidos.ColumnasFormasDePago.NOMBRE)

    //@GET BY ID Cabecera Pedido
    fun obtenerCabeceraPedidoPorId(id: String): Cursor {
        val db = baseDatos!!.readableDatabase

        val selection = String.format("%s=?", ContratoPedidos.ColumnasCabeceraPedido.ID)
        val selectionArgs = arrayOf(id)

        val builder = SQLiteQueryBuilder()
        builder.tables = CABECERA_PEDIDO_JOIN_CLIENTE_Y_FORMA_PAGO

        val proyeccion = arrayOf<String>(BaseDatosPedidos.Tablas.CABECERA_PEDIDO + "." + ContratoPedidos.ColumnasCabeceraPedido.ID,
                ContratoPedidos.ColumnasCabeceraPedido.FECHA, ContratoPedidos.ColumnasCliente.NOMBRES,
                ContratoPedidos.ColumnasCliente.APELLLIDOS, ContratoPedidos.ColumnasFormasDePago.NOMBRE)


        return builder.query(db, proyeccion, selection, selectionArgs, null, null, null)
    }

    //@POST Cabecera Pedido
    fun insertatCabeceraPedido(pedido: CabeceraPedido): String {

        val db = baseDatos!!.writableDatabase

        //generate PK
        val idCabeceraPedido = ContratoPedidos.CabeceraPedido.generarIdCabeceraPedido()

        val valores = ContentValues()

        valores.put(ContratoPedidos.ColumnasCabeceraPedido.ID, idCabeceraPedido)
        valores.put(ContratoPedidos.ColumnasCabeceraPedido.FECHA, pedido.fecha)
        valores.put(ContratoPedidos.ColumnasCabeceraPedido.ID_CLIENTE, pedido.idCliente)
        valores.put(ContratoPedidos.ColumnasCabeceraPedido.ID_FORMA_PAGO, pedido.idFormaPago)

        // Insertar cabecera
        db.insertOrThrow(BaseDatosPedidos.Tablas.CABECERA_PEDIDO, null, valores)

        return idCabeceraPedido
    }

    //@PUT Cabecera Pedido
    fun actualizarCabeceraPedido(pedidoNuevo: CabeceraPedido): Boolean {


        val db = baseDatos!!.writableDatabase

        val valores = ContentValues()

        valores.put(ContratoPedidos.ColumnasCabeceraPedido.FECHA, pedidoNuevo.fecha)
        valores.put(ContratoPedidos.ColumnasCabeceraPedido.ID_CLIENTE, pedidoNuevo.idCliente)
        valores.put(ContratoPedidos.ColumnasCabeceraPedido.ID_FORMA_PAGO, pedidoNuevo.idFormaPago)

        val whereClause = String.format("%s=?", ContratoPedidos.ColumnasCabeceraPedido.ID)
        val whereArgs = arrayOf<String>(pedidoNuevo.idCabeceraPedido!!)

        val resultado = db.update(BaseDatosPedidos.Tablas.CABECERA_PEDIDO, valores, whereClause, whereArgs)

        return resultado > 0
    }

    //@DELETE Cabecera Pedido
    fun eliminarCabeceraPedido(idCabeceraPedido: String): Boolean {


        val db = baseDatos!!.writableDatabase


        val whereClause = ContratoPedidos.ColumnasCabeceraPedido.ID + " =? "
        val whereArgs = arrayOf(idCabeceraPedido)

        val resultado = db.delete(BaseDatosPedidos.Tablas.CABECERA_PEDIDO, whereClause, whereArgs)

        return resultado > 0
    }


    /** [ContratoPedidos.DetallePedido]
     * OPERACIONES
     */
    fun obtenerDetallePedido(): Cursor {
        val db = baseDatos!!.readableDatabase

        val sql = String.format("SELECT * FROM %s ",
                BaseDatosPedidos.Tablas.DETALLE_PEDIDO)


        return db.rawQuery(sql, null)

    }

    //@GET BY idCabeceraPedido DetallePedido
    fun obtenerDetallePedidoPorIdPedido(idCabeceraPedido: String): Cursor {
        val db = baseDatos!!.readableDatabase

        val sql = String.format("SELECT * FROM %s WHERE %s=?",
                BaseDatosPedidos.Tablas.DETALLE_PEDIDO, ContratoPedidos.ColumnasDetallePedido.ID)


        val selectionArgs = arrayOf(idCabeceraPedido)



        return db.rawQuery(sql, selectionArgs)
    }

    //@POST DetallePedido
    fun insertatDetallePedido(detalle: DetallePedido): String {

        val db = baseDatos!!.writableDatabase


        val valores = ContentValues()

        valores.put(ContratoPedidos.ColumnasDetallePedido.ID, detalle.idCabeceraPedido)
        valores.put(ContratoPedidos.ColumnasDetallePedido.SECUENCIA, detalle.secuencia)
        valores.put(ContratoPedidos.ColumnasDetallePedido.ID_PRODUCTO, detalle.idProducto)
        valores.put(ContratoPedidos.ColumnasDetallePedido.CANTIDAD, detalle.cantidad)
        valores.put(ContratoPedidos.ColumnasDetallePedido.PRECIO, detalle.precio)


        // Insertar DetallePedido
        db.insertOrThrow(BaseDatosPedidos.Tablas.DETALLE_PEDIDO, null, valores)

        return String.format("%s#%d", detalle.idCabeceraPedido, detalle.secuencia)
    }

    //@PUT DetallePedido
    fun actualizarDetallePedido(detalleNuevo: DetallePedido): Boolean {


        val db = baseDatos!!.writableDatabase

        val valores = ContentValues()

        valores.put(ContratoPedidos.ColumnasDetallePedido.SECUENCIA, detalleNuevo.secuencia)
        valores.put(ContratoPedidos.ColumnasDetallePedido.CANTIDAD, detalleNuevo.cantidad)
        valores.put(ContratoPedidos.ColumnasDetallePedido.PRECIO, detalleNuevo.precio)

        val whereClause = String.format("%s=? AND %s=? ",
                ContratoPedidos.ColumnasDetallePedido.ID, ContratoPedidos.ColumnasDetallePedido.SECUENCIA)
        val whereArgs = arrayOf<String>(detalleNuevo.idCabeceraPedido!!, detalleNuevo.secuencia as String)

        val resultado = db.update(BaseDatosPedidos.Tablas.DETALLE_PEDIDO, valores, whereClause, whereArgs)

        return resultado > 0
    }

    //@DELETE DetallePedido
    fun eliminarDetallePedido(idCabeceraPedido: String, secuencia: Int?): Boolean {


        val db = baseDatos!!.writableDatabase


        val whereClause = String.format("%s=? AND %s=? ",
                ContratoPedidos.ColumnasDetallePedido.ID, ContratoPedidos.ColumnasDetallePedido.SECUENCIA)
        val whereArgs = arrayOf(idCabeceraPedido, secuencia.toString())

        val resultado = db.delete(BaseDatosPedidos.Tablas.DETALLE_PEDIDO, whereClause, whereArgs)

        return resultado > 0
    }


    /** [ContratoPedidos.Producto]
     * OPERACIONES
     */
    //@GET BY ALL Producto
    fun obtenerProducto(): Cursor {
        val db = baseDatos!!.readableDatabase

        val sql = String.format("SELECT * FROM %s ",
                BaseDatosPedidos.Tablas.PRODUCTO)

        return db.rawQuery(sql, null)
    }


    //@GET BY idProducto Producto
    fun obtenerProductoIdProducto(idProducto: String): Cursor {
        val db = baseDatos!!.readableDatabase

        val sql = String.format("SELECT * FROM %s WHERE %s=?",
                BaseDatosPedidos.Tablas.PRODUCTO, ContratoPedidos.ColumnasProducto.ID)


        val selectionArgs = arrayOf(idProducto)



        return db.rawQuery(sql, selectionArgs)
    }

    //@POST Producto
    fun insertatProducto(newProducto: Producto): String {

        val db = baseDatos!!.writableDatabase

        // Generar PK
        val idProducto = ContratoPedidos.Producto.generarIdProducto()


        val valores = ContentValues()

        valores.put(ContratoPedidos.ColumnasProducto.ID, idProducto)
        valores.put(ContratoPedidos.ColumnasProducto.NOMBRE, newProducto.nombre)
        valores.put(ContratoPedidos.ColumnasProducto.PRECIO, newProducto.precio)
        valores.put(ContratoPedidos.ColumnasProducto.EXISTENCIAS, newProducto.existencias)


        // Insertar DetallePedido
        db.insertOrThrow(BaseDatosPedidos.Tablas.PRODUCTO, null, valores)

        return idProducto
    }

    //@PUT Producto
    fun actualizarProducto(editProducto: Producto): Boolean {


        val db = baseDatos!!.writableDatabase

        val valores = ContentValues()

        valores.put(ContratoPedidos.ColumnasProducto.NOMBRE, editProducto.nombre)
        valores.put(ContratoPedidos.ColumnasProducto.PRECIO, editProducto.precio)
        valores.put(ContratoPedidos.ColumnasProducto.EXISTENCIAS, editProducto.existencias)

        val whereClause = String.format("%s=? ",
                ContratoPedidos.ColumnasProducto.ID)
        val whereArgs = arrayOf<String>(editProducto.idProducto!!)

        val resultado = db.update(BaseDatosPedidos.Tablas.PRODUCTO, valores, whereClause, whereArgs)

        return resultado > 0
    }

    //@DELETE Producto
    fun eliminarProducto(idProducto: String): Boolean {


        val db = baseDatos!!.writableDatabase


        val whereClause = String.format("%s=? ",
                ContratoPedidos.ColumnasProducto.ID)
        val whereArgs = arrayOf(idProducto)

        val resultado = db.delete(BaseDatosPedidos.Tablas.PRODUCTO, whereClause, whereArgs)

        return resultado > 0
    }


    /** [ContratoPedidos.Cliente]
     * OPERACIONES
     */
    //@GET BY ALL Cliente
    fun obtenerCliente(): Cursor {
        val db = baseDatos!!.readableDatabase

        val sql = String.format("SELECT * FROM %s ",
                BaseDatosPedidos.Tablas.CLIENTE)


        return db.rawQuery(sql, null)
    }


    //@GET BY idCliente Cliente
    fun obtenerClienteIdCliente(idCliente: String): Cursor {
        val db = baseDatos!!.readableDatabase

        val sql = String.format("SELECT * FROM %s WHERE %s=?",
                BaseDatosPedidos.Tablas.CLIENTE, ContratoPedidos.ColumnasCliente.ID)


        val selectionArgs = arrayOf(idCliente)



        return db.rawQuery(sql, selectionArgs)
    }

    //@POST Cliente
    fun insertatCliente(newCliente: Cliente): String {

        val db = baseDatos!!.writableDatabase

        // Generar PK
        val idCliente = ContratoPedidos.Cliente.generarIdCliente()


        val valores = ContentValues()

        valores.put(ContratoPedidos.ColumnasCliente.ID, idCliente)
        valores.put(ContratoPedidos.ColumnasCliente.NOMBRES, newCliente.nombres)
        valores.put(ContratoPedidos.ColumnasCliente.APELLLIDOS, newCliente.apellidos)
        valores.put(ContratoPedidos.ColumnasCliente.TELEFONO, newCliente.telefono)


        // Insertar DetallePedido
        db.insertOrThrow(BaseDatosPedidos.Tablas.CLIENTE, null, valores)

        return idCliente
    }

    //@PUT Cliente
    fun actualizarCliente(editCliente: Cliente): Boolean {


        val db = baseDatos!!.writableDatabase

        val valores = ContentValues()

        valores.put(ContratoPedidos.ColumnasCliente.NOMBRES, editCliente.nombres)
        valores.put(ContratoPedidos.ColumnasCliente.APELLLIDOS, editCliente.apellidos)
        valores.put(ContratoPedidos.ColumnasCliente.TELEFONO, editCliente.telefono)

        val whereClause = String.format("%s=? ",
                ContratoPedidos.ColumnasCliente.ID)
        val whereArgs = arrayOf<String>(editCliente.idCliente!!)

        val resultado = db.update(BaseDatosPedidos.Tablas.CLIENTE, valores, whereClause, whereArgs)

        return resultado > 0
    }

    //@DELETE Cliente
    fun eliminarCliente(idCliente: String): Boolean {


        val db = baseDatos!!.writableDatabase


        val whereClause = String.format("%s=? ",
                ContratoPedidos.ColumnasCliente.ID)
        val whereArgs = arrayOf(idCliente)

        val resultado = db.delete(BaseDatosPedidos.Tablas.CLIENTE, whereClause, whereArgs)

        return resultado > 0
    }

    /** [ContratoPedidos.FormasDePago]
     * OPERACIONES
     */
    //@GET BY ALL FormaPago
    fun obtenerFormaPago(): Cursor {
        val db = baseDatos!!.readableDatabase

        val sql = String.format("SELECT * FROM %s ",
                BaseDatosPedidos.Tablas.FORMA_PAGO)


        return db.rawQuery(sql, null)
    }


    //@GET BY idFormaPago FormaPago
    fun obtenerFormaPagoIdFormaPago(idFormaPago: String): Cursor {
        val db = baseDatos!!.readableDatabase

        val sql = String.format("SELECT * FROM %s WHERE %s=?",
                BaseDatosPedidos.Tablas.FORMA_PAGO, ContratoPedidos.ColumnasProducto.ID)


        val selectionArgs = arrayOf(idFormaPago)



        return db.rawQuery(sql, selectionArgs)
    }

    //@POST FormasDePago
    fun insertatFormasDePago(newFormaPago: FormaPago): String {

        val db = baseDatos!!.writableDatabase

        // Generar PK
        val idFormaPago = ContratoPedidos.formasDePago.generarIdFormasDePago()


        val valores = ContentValues()

        valores.put(ContratoPedidos.ColumnasFormasDePago.ID, idFormaPago)
        valores.put(ContratoPedidos.ColumnasFormasDePago.NOMBRE, newFormaPago.nombre)


        // Insertar DetallePedido
        db.insertOrThrow(BaseDatosPedidos.Tablas.FORMA_PAGO, null, valores)

        return idFormaPago
    }

    //@PUT FormasDePago
    fun actualizarFormasDePago(editFormasDePago: FormaPago): Boolean {


        val db = baseDatos!!.writableDatabase

        val valores = ContentValues()

        valores.put(ContratoPedidos.ColumnasFormasDePago.NOMBRE, editFormasDePago.nombre)


        val whereClause = String.format("%s=? ",
                ContratoPedidos.ColumnasFormasDePago.ID)
        val whereArgs = arrayOf<String>(editFormasDePago.idFormaPago!!)

        val resultado = db.update(BaseDatosPedidos.Tablas.FORMA_PAGO, valores, whereClause, whereArgs)

        return resultado > 0
    }

    //@DELETE FormasDePago
    fun eliminarFormasDePago(idFormasDePago: String): Boolean {


        val db = baseDatos!!.writableDatabase


        val whereClause = String.format("%s=? ",
                ContratoPedidos.ColumnasFormasDePago.ID)
        val whereArgs = arrayOf(idFormasDePago)

        val resultado = db.delete(BaseDatosPedidos.Tablas.FORMA_PAGO, whereClause, whereArgs)

        return resultado > 0
    }

    fun getDb(): SQLiteDatabase {
        return baseDatos!!.writableDatabase
    }


}