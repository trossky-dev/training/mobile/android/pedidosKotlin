package com.dev.trossky.kotlinpedidos.sqlite

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.os.Build
import android.provider.BaseColumns

/**
 * Created by luis on 3/10/17.
 */
class BaseDatosPedidos : SQLiteOpenHelper {
    companion object {
        private val NOMBRE_BASE_DATOS = "pedidos.db"
        private val VERSION_ACTUAL = 1
    }

    interface Tablas{
        companion object {
            val CABECERA_PEDIDO="cabecera_pedido"
            val DETALLE_PEDIDO="detalle_pedido"
            val PRODUCTO="producto"
            val CLIENTE="cliente"
            val FORMA_PAGO="forma_pago"

        }
    }
    interface Referencias{
        companion object {
            val ID_CABECERA_PEDIDO="REFERENCES ${Tablas.CABECERA_PEDIDO} (${ContratoPedidos.ColumnasCabeceraPedido.ID}) ON DELETE CASCADE"
            val ID_PROUCTO="REFERENCES ${Tablas.PRODUCTO} (${ContratoPedidos.ColumnasProducto.ID})"
            val ID_CLIENTE="REFERENCES ${Tablas.CLIENTE} (${ContratoPedidos.ColumnasCliente.ID})"
            val ID_FORMA_PAGO="REFERENCES ${Tablas.FORMA_PAGO} (${ContratoPedidos.ColumnasFormasDePago.ID})"



        }
    }
     private var contexto: Context


    constructor(contexto: Context) : super(contexto,NOMBRE_BASE_DATOS,null, VERSION_ACTUAL) {
        this.contexto = contexto
    }




    override fun onOpen(db: SQLiteDatabase?) {
        super.onOpen(db)
        if (db != null) {
            if (!db.isReadOnly()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {

                    db.setForeignKeyConstraintsEnabled(true)
                } else {
                    db.execSQL("PRAGMA foreign_keys=ON")
                }
            }
        }



    }

    override fun onCreate(db: SQLiteDatabase?) {


        if (db != null) {

            db.execSQL((String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "%s TEXT UNIQUE NOT NULL, %s DATETIME NOT NULL, %s TEXT NOT NULL %s, "+
                    "%s TEXT NOT NULL %s) ",
                    Tablas.CABECERA_PEDIDO, BaseColumns._ID,
                    ContratoPedidos.ColumnasCabeceraPedido.ID, ContratoPedidos.ColumnasCabeceraPedido.FECHA,
                    ContratoPedidos.ColumnasCabeceraPedido.ID_CLIENTE,Referencias.ID_CLIENTE,
                    ContratoPedidos.ColumnasCabeceraPedido.ID_FORMA_PAGO, Referencias.ID_FORMA_PAGO )))

            db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "%s TEXT  NOT NULL %s, %s INTEGER  NOT NULL CHECK (%s >0), " +
                    "%s INTEGER NOT NULL %s, " +
                    "%s INTEGER NOT NULL, " +
                    "%s REAL NOT NULL, UNIQUE(%s,%s) ) ",
                    Tablas.DETALLE_PEDIDO, BaseColumns._ID,
                    ContratoPedidos.ColumnasDetallePedido.ID, Referencias.ID_CABECERA_PEDIDO,
                    ContratoPedidos.ColumnasDetallePedido.SECUENCIA, ContratoPedidos.ColumnasDetallePedido.SECUENCIA,
                    ContratoPedidos.ColumnasDetallePedido.ID_PRODUCTO, Referencias.ID_PROUCTO,
                    ContratoPedidos.ColumnasDetallePedido.CANTIDAD, ContratoPedidos.ColumnasDetallePedido.PRECIO,
                    ContratoPedidos.ColumnasDetallePedido.ID, ContratoPedidos.ColumnasDetallePedido.SECUENCIA))

            db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "%s TEXT NOT NULL UNIQUE, %s TEXT NOT NULL, " +
                    "%s REAL NOT NULL , "+
                    "%s INTEGER NOT NULL CHECK(%S>=0) ) ",
                    Tablas.PRODUCTO, BaseColumns._ID,
                    ContratoPedidos.ColumnasProducto.ID, ContratoPedidos.ColumnasProducto.NOMBRE,
                    ContratoPedidos.ColumnasProducto.PRECIO,
                    ContratoPedidos.ColumnasProducto.EXISTENCIAS, ContratoPedidos.ColumnasProducto.EXISTENCIAS ));

            db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "%s TEXT  NOT NULL UNIQUE, %s TEXT NOT NULL, " +
                    "%s TEXT NOT NULL , "+
                    "%s  ) ",
                    Tablas.CLIENTE, BaseColumns._ID,
                    ContratoPedidos.ColumnasCliente.ID, ContratoPedidos.ColumnasCliente.NOMBRES,
                    ContratoPedidos.ColumnasCliente.APELLLIDOS, ContratoPedidos.ColumnasCliente.TELEFONO ));

            db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "%s TEXT  NOT NULL UNIQUE, %s TEXT NOT NULL ) ",
                    Tablas.FORMA_PAGO, BaseColumns._ID,
                    ContratoPedidos.ColumnasFormasDePago.ID,ContratoPedidos.ColumnasFormasDePago.NOMBRE))









        }
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {


        if (db != null) {
            db.execSQL("DROP TABLE IF EXISTS " + Tablas.CABECERA_PEDIDO)
            db.execSQL("DROP TABLE IF EXISTS " + Tablas.DETALLE_PEDIDO)
            db.execSQL("DROP TABLE IF EXISTS " + Tablas.PRODUCTO)
            db.execSQL("DROP TABLE IF EXISTS " + Tablas.CLIENTE)
            db.execSQL("DROP TABLE IF EXISTS " + Tablas.FORMA_PAGO)
            onCreate(db)
        }



    }
}