package com.dev.trossky.kotlinpedidos

import android.app.Activity
import android.database.DatabaseUtils
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import com.dev.trossky.kotlinpedidos.model.*
import com.dev.trossky.kotlinpedidos.sqlite.OperacionesBaseDatos
import java.util.*

class MainActivity : Activity() {

     companion object {
         lateinit var datos: OperacionesBaseDatos
     }




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        applicationContext.deleteDatabase("pedidos.db")


        datos = OperacionesBaseDatos.obtenerInstancia(applicationContext)

        TareaPruebaDatos().execute()
    }

    inner class TareaPruebaDatos : AsyncTask<Void, Void, Void>() {


        override fun doInBackground(vararg voids: Void): Void? {

            //[POST]
            val fechaActual = Calendar.getInstance().time.toString()

            try {
                datos.getDb().beginTransaction()//Start

                //Insercion Clientes
                val cliente1 = datos.insertatCliente(Cliente( null,"Mathias", "Garcia", "32229534"))
                val cliente2 = datos.insertatCliente(Cliente(null,"Freddy Alexander", "Garcia", "310752015"))

                //Insercion Formas de Pago
                val formapago1 = datos.insertatFormasDePago(FormaPago( "Efectivo"))
                val formapago2 = datos.insertatFormasDePago(FormaPago("Credito"))

                //insersion Producto
                val producto1 = datos.insertatProducto(Producto( "Manzana Unidad", 2f, 100))
                val producto2 = datos.insertatProducto(Producto( "Pera Unidad", 3f, 230))
                val producto3 = datos.insertatProducto(Producto( "Guayaba Unidad", 5f, 55))
                val producto4 = datos.insertatProducto(Producto( "Mani Unidad", 3.6f, 60))

                //insercion Pedido
                val pedido1 = datos.insertatCabeceraPedido(CabeceraPedido( fechaActual, cliente1, formapago1))
                val pedido2 = datos.insertatCabeceraPedido(CabeceraPedido( fechaActual, cliente2, formapago2))

                //insercion Detalle Pedido
                datos.insertatDetallePedido(DetallePedido(pedido1, 1, producto1, 5, 2f))
                datos.insertatDetallePedido(DetallePedido(pedido1, 2, producto2, 10, 3f))
                datos.insertatDetallePedido(DetallePedido(pedido2, 1, producto3, 30, 5f))
                datos.insertatDetallePedido(DetallePedido(pedido2, 2, producto4, 20, 3.6f))

                // Eliminar Detalle
                datos.eliminarCabeceraPedido(pedido1)


                //Actualizar CLiente
                datos.actualizarCliente(Cliente(cliente2, "Junior", "Garcia", "7785421"))



                datos.getDb().setTransactionSuccessful()//end
            } catch (e: Exception) {
                e.printStackTrace()
                datos.getDb().endTransaction()
            }

            // [QUERIES]
            /**
             * La clase DatabaseUtils es para concatenar sentencias, ligar parámetros a comandos,
             * construir condiciones para la cláusula WHERE, etc. En mi caso usé le método
             * dumbCursor() para mostrar de forma presentable las filas de los cursores que
             * vaya a loguear.
             */
            Log.d("Clientes", "Clientes")
            DatabaseUtils.dumpCursor(datos.obtenerCliente())
            Log.d("Formas de Pago", "Formas de Pago")
            DatabaseUtils.dumpCursor(datos.obtenerFormaPago())
            Log.d("Producto", "Producto")
            DatabaseUtils.dumpCursor(datos.obtenerProducto())
            Log.d("Pedido", "Pedido")
            DatabaseUtils.dumpCursor(datos.obtenerCabeceraPedido())
            Log.d("Detalle Pedido", "Detalle Pedido")
            DatabaseUtils.dumpCursor(datos.obtenerDetallePedido())


            return null
        }


    }
}
