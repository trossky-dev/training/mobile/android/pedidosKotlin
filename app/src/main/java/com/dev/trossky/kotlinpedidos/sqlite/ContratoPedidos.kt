package com.dev.trossky.kotlinpedidos.sqlite

import android.database.sqlite.SQLiteOpenHelper
import android.net.Uri
import java.util.*

/**
 * Clase que administra la conexión de la base de datos y su estructuración
 */

class ContratoPedidos  {

    // [/URIS]
    companion object {
        val AUTORIDAD_CONTENIDO = "com.trossky-dev.pedidos"
        val URI_BASE = Uri.parse("content://" + AUTORIDAD_CONTENIDO)
        //URIS
        private val RUTA_PEDIDOS = "cabeceras_pedidos"
        private val RUTA_DETALLES_PEDIDO = "detalles_pedido"
        private val RUTA_PRODUCTOS = "productos"
        private val RUTA_CLIENTES = "clientes"
        private val RUTA_FORMAS_PAGO = "formas_pago"

        // [/URIS]


        // [TIPOS_MIME]
        val BASE_CONTENIDOS = "pedidos."
        val TIPO_CONTENIDO = "vnd.android.cursor.dir/vnd." + BASE_CONTENIDOS
        val TIPO_CONTENIDO_ITEM = "vnd.android.cursor.item/vnd." + BASE_CONTENIDOS

        fun generarMime(id: String?): String? {
            if (id != null) {
                return TIPO_CONTENIDO + id
            } else {
                return null
            }
        }

        fun generarMimeItem(id: String?): String? {
            if (id != null) {
                return TIPO_CONTENIDO_ITEM + id
            } else {
                return null
            }
        }
        // [/TIPOS_MIME]
    }

     interface ColumnasCabeceraPedido {
        companion object {
            val ID = "id"
            val FECHA = "fecha"
            val ID_CLIENTE = "id_cliente"
            val ID_FORMA_PAGO = "id_forma_pago"
        }
    }

     interface ColumnasDetallePedido {
        companion object {
            val ID = "id"
            val SECUENCIA = "secuencia"
            val ID_PRODUCTO = "id_producto"
            val CANTIDAD = "cantidad"
            val PRECIO = "precio"
        }
    }

     interface ColumnasProducto {
        companion object {
            val ID = "id"
            val NOMBRE = "nombre"
            val PRECIO = "precio"
            val EXISTENCIAS = "existencias"
        }
    }

     interface ColumnasCliente {
        companion object {
            val ID = "id"
            val NOMBRES = "nombres"
            val APELLLIDOS = "apellidos"
            val TELEFONO = "telefono"
        }
    }

     interface ColumnasFormasDePago {
        companion object {
            val ID = "id"
            val NOMBRE = "nombre"
        }
    }

    class CabeceraPedido : ColumnasCabeceraPedido {
        companion object {

            val URI_CONTENIDO = URI_BASE.buildUpon().appendPath(RUTA_PEDIDOS).build()

            val PARAMETRO_FILTRO = "filtro"
            val FILTRO_CLIENTE = "cliente"
            val FILTRO_FECHA = "fecha"
            val FILTRO_TOTAL = "total"

            /**
             * Obtener el valor de id, que se envia en la uri, como un PathParams
             * @param 'pedidos/23'
             * *
             * @return 23
             * *
             */
            fun obtenerIdCabeceraPedido(uri: Uri): String {
                return uri.pathSegments[0]
            }

            //URI  /cabeceras_pedidos/*
            fun crearUriCabeceraPedido(id: String): Uri {
                return URI_CONTENIDO.buildUpon().appendPath(id).build()
            }


            //  URI /cabeceras_pedidos/*/detalles
            fun crearUriParaDetalles(id: String): Uri {
                return URI_CONTENIDO.buildUpon().appendPath(id).appendPath("detalles").build()
            }

            // verificar si tiene la uri algun @QueryParmas
            fun tieneFiltro(uri: Uri?): Boolean {
                return uri != null && uri.getQueryParameter(PARAMETRO_FILTRO) != null
            }

            fun generarIdCabeceraPedido(): String {
                return "CP-" + UUID.randomUUID().toString()
            }
        }

    }

    class DetallePedido : ColumnasDetallePedido{
        companion object {
            val URI_CONTENIDO:Uri= URI_BASE.buildUpon().appendPath(RUTA_DETALLES_PEDIDO).build()
            //   Uri de la forma 'detalles_pedido/:id#:secuencia'
            fun crearUriParaDetallePedido(id:String,secuencia:String):Uri{
                return URI_CONTENIDO.buildUpon().appendPath("${id}#${secuencia}").build()
            }

            /**
             * Obtener el valor de id, que se envia en la uri, como un PathParams
             * @param uri 'detalles_pedido/23#1'
             * @return [23,1]
             * */

            fun obtenerIdDetallePedido( uri: Uri): Array<String>{

                return uri.lastPathSegment.split("#".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            }

        }
    }

    class Producto : ColumnasProducto{
        companion object {

            //   Uri de la forma '/productos'
            val URI_CONTENIDO:Uri= URI_BASE.buildUpon().appendPath(RUTA_PRODUCTOS).build()

            //   Uri de la forma '/productos/:id'
            fun crearUriProducto(id: String):Uri{
                return URI_CONTENIDO.buildUpon().appendPath(id).build()
            }

            /**
             *    Uri de la forma '/productos/:id'
             * Obtener el valor de id, que se envia en la uri, como un PathParams
             * @param uri 'productos/4'
             * @return 4
             * */
            fun obtenerIdProducto(uri:Uri):String{
                return uri.lastPathSegment
            }

            fun generarIdProducto():String{
                return "PRO-"+ UUID.randomUUID().toString();
            }

        }
    }

    class Cliente: ColumnasCliente{
        companion object {

            //   Uri de la forma '/clientes'
            val URI_CONTENIDO:Uri= URI_BASE.buildUpon().appendPath(RUTA_CLIENTES).build()

            //   Uri de la forma '/clientes/:id'
            fun crearUriCliente(id:String):Uri{
                return URI_CONTENIDO.buildUpon().appendPath(id).build()
            }

            /**
             *    Uri de la forma '/clientes/:id'
             * Obtener el valor de id, que se envia en la uri, como un PathParams
             * @param uri 'clientes/4'
             * @return 4
             * */
            fun obtenerIdCliente(uri:Uri):String{
                return uri.lastPathSegment
            }

            fun generarIdCliente():String{
                return "CLI-${UUID.randomUUID().toString()}"
            }

        }
    }

    class formasDePago(cFP: ColumnasFormasDePago) :ColumnasFormasDePago by cFP{
        companion object {
            //   Uri de la forma '/formas_pago'
            val URI_CONTENIDO:Uri= URI_BASE.buildUpon().appendPath(RUTA_FORMAS_PAGO).build()

            //   Uri de la forma '/formas_pago/:id'
            fun crearUriFormaPago(id:String):Uri{
                return URI_CONTENIDO.buildUpon().appendPath(id).build()
            }

            /**
             *    Uri de la forma '/formas_pago/:id'
             * Obtener el valor de id, que se envia en la uri, como un PathParams
             * @param uri 'formas_pago/4'
             * @return 4
             * */
            fun obtenerIdFormaPago(uri:Uri):String{
                return uri.lastPathSegment
            }

            fun generarIdFormasDePago():String{
                return "FP-${UUID.randomUUID().toString()}"
            }


        }
    }

    constructor(){

    }

}