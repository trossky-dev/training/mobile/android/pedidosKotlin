package com.dev.trossky.kotlinpedidos.model

/**
 * Created by luis on 3/10/17.
 */
class DetallePedido (secuencia: Int,idProducto: String,cantidad: Int?,precio: Float?){
    var idCabeceraPedido: String?=null
    var secuencia: Int? = secuencia
    var idProducto: String=idProducto
    var cantidad: Int? = cantidad
    var precio: Float? = precio


    constructor(idCabeceraPedido: String,secuencia: Int?,idProducto: String,cantidad: Int?,precio: Float?):this(secuencia!!, idProducto, cantidad,precio){
        this.idCabeceraPedido=idCabeceraPedido
        this.secuencia=secuencia
        this.idProducto=idProducto
        this.cantidad=cantidad
        this.precio=precio

    }
}