package com.dev.trossky.kotlinpedidos.model

/**
 * Created by luis on 3/10/17.
 */
class Producto ( nombre: String, precio: Float, existencias: Int){

    var idProducto: String?=null
    var nombre: String=nombre
    var precio: Float = precio
    var existencias: Int = existencias

    constructor(idProducto: String, nombre: String, precio: Float, existencias: Int):this(nombre, precio, existencias){
        this.idProducto=idProducto
        this.nombre=nombre
        this.precio=precio
        this.existencias=existencias
    }



}
