package com.dev.trossky.kotlinpedidos.sqlite

import android.content.ContentProvider

import android.content.ContentResolver
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.database.sqlite.SQLiteBlobTooBigException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteQuery
import android.database.sqlite.SQLiteQueryBuilder
import android.net.Uri



class ProviderPedidos : ContentProvider() {

    // [URI_MATCHER]
    private lateinit var db:BaseDatosPedidos

    companion object {

        private lateinit var resolver:ContentResolver


        // Casos
        val CABECERAS_PEDIDOS:Int= 100
        val CABECERAS_PEDIDOS_ID = 101
        val CABECERAS_ID_DETALLES = 102

        val DETALLES_PEDIDOS = 200
        val DETALLES_PEDIDOS_ID = 201

        val PRODUCTOS = 300
        val PRODUCTOS_ID = 301

        val CLIENTES = 400
        val CLIENTES_ID = 401

        val FORMAS_PAGO = 500
        val FORMAS_PAGO_ID = 501

        val AUTORIDAD = "com.trossky-dev.pedidos"


        var uriMatcher: UriMatcher= UriMatcher(UriMatcher.NO_MATCH)

        init {
            uriMatcher.addURI(AUTORIDAD,"cabeceras_pedidos", CABECERAS_PEDIDOS)
            uriMatcher.addURI(AUTORIDAD,"cabeceras_pedidos/*", CABECERAS_PEDIDOS_ID)
            uriMatcher.addURI(AUTORIDAD,"cabeceras_pedidos/*/detalles", CABECERAS_ID_DETALLES)

            uriMatcher.addURI(AUTORIDAD,"detalles_pedidos", DETALLES_PEDIDOS)
            uriMatcher.addURI(AUTORIDAD,"detalles_pedidos/*", DETALLES_PEDIDOS_ID)

            uriMatcher.addURI(AUTORIDAD,"productos", PRODUCTOS)
            uriMatcher.addURI(AUTORIDAD,"productos/*", PRODUCTOS_ID)

            uriMatcher.addURI(AUTORIDAD,"clientes", CLIENTES)
            uriMatcher.addURI(AUTORIDAD,"clientes/*", CLIENTES_ID)

            uriMatcher.addURI(AUTORIDAD,"formas_pago", FORMAS_PAGO)
            uriMatcher.addURI(AUTORIDAD,"formas_pago/*", FORMAS_PAGO_ID)
        }


    }







    override fun onCreate(): Boolean {
        db= BaseDatosPedidos(context)
        resolver=context.contentResolver
        return true
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
        // Implement this to handle requests to delete one or more rows.
        throw UnsupportedOperationException("Not yet implemented")
    }

    override fun getType(uri: Uri): String? {

        // at the given URI.

        when(uriMatcher.match(uri)){

            CABECERAS_PEDIDOS-> return ContratoPedidos.generarMime("cabeceras_pedidos")
            CABECERAS_PEDIDOS_ID-> return ContratoPedidos.generarMimeItem("cabeceras_pedidos")
            //CABECERAS_ID_DETALLES-> return ContratoPedidos.generarMimeItem("cabeceras_pedidos")

            DETALLES_PEDIDOS-> return ContratoPedidos.generarMime("detalles_pedidos")
            DETALLES_PEDIDOS_ID-> return ContratoPedidos.generarMimeItem("detalles_pedidos")

            PRODUCTOS-> return ContratoPedidos.generarMime("productos")
            PRODUCTOS_ID-> return ContratoPedidos.generarMimeItem("productos")

            CLIENTES-> return ContratoPedidos.generarMime("clientes")
            CLIENTES_ID-> return ContratoPedidos.generarMimeItem("clientes")

            FORMAS_PAGO-> return ContratoPedidos.generarMime("formas_pago")
            FORMAS_PAGO_ID-> return ContratoPedidos.generarMimeItem("formas_pago")

            else->{
                throw UnsupportedOperationException("Uri desconocida => ${uri}")
            }


        }
        throw UnsupportedOperationException("Not yet implemented")
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        // TODO: Implement this to handle requests to insert a new row.
        throw UnsupportedOperationException("Not yet implemented")
    }

    private val CABECERA_PEDIDO_JOIN_CLIENTE_Y_FORMA_PAGO = "cabecera_pedido " +
            "INNER JOIN cliente " +
            "ON cabecera_pedido.id_cliente= cliente.id " +
            "INNER JOIN forma_pago " +
            "ON cabecera_pedido.id_forma_pago=forma_pago.id "

    private val proyCabeceraPedido = arrayOf<String>(BaseDatosPedidos.Tablas.CABECERA_PEDIDO + "." + ContratoPedidos.ColumnasCabeceraPedido.ID, ContratoPedidos.ColumnasCabeceraPedido.FECHA, ContratoPedidos.ColumnasCliente.NOMBRES, ContratoPedidos.ColumnasCliente.APELLLIDOS, ContratoPedidos.ColumnasFormasDePago.NOMBRE)


    override fun query(uri: Uri, projection: Array<String>?, selection: String?,
                       selectionArgs: Array<String>?, sortOrder: String?): Cursor? {

        // Obtener base de datos
        var db:SQLiteDatabase =this.db.readableDatabase
        var match:Int= uriMatcher.match(uri)

        var c:Cursor

        when(match){
            CABECERAS_PEDIDOS-> {

                // Obtener filtro
                val filtro = if (ContratoPedidos.CabeceraPedido.tieneFiltro(uri))
                    construirFiltro(uri.getQueryParameter("filtro"))
                else
                    null

                var builder = SQLiteQueryBuilder()

                // Consultando todas las cabeceras de pedido
                builder.tables = CABECERA_PEDIDO_JOIN_CLIENTE_Y_FORMA_PAGO
                return builder.query(db, proyCabeceraPedido, null, null, null, null, null)

            }
            else->{
                throw UnsupportedOperationException("Uri no soportada")
            }
        }

        c.setNotificationUri(resolver,uri)


        return c
    }

    override fun update(uri: Uri, values: ContentValues?, selection: String?,
                        selectionArgs: Array<String>?): Int {
        // TODO: Implement this to handle requests to update one or more rows.
        throw UnsupportedOperationException("Not yet implemented")
    }

    private fun construirFiltro(filtro: String): String {
        var sentencia: String? = null

        when (filtro) {
            ContratoPedidos.CabeceraPedido.FILTRO_CLIENTE -> sentencia = "cliente.nombres"

            ContratoPedidos.CabeceraPedido.FILTRO_FECHA -> sentencia = "cabecera_pedido.fecha"
        }

        return sentencia!!
    }
}
