package com.dev.trossky.kotlinpedidos.model

/**
 * Created by luis on 3/10/17.
 */
class Cliente (idCliente: String?,
               nombres: String,
               apellidos: String,
               telefono: String) {

    var idCliente: String?=idCliente
    var nombres: String=nombres
    var apellidos: String=apellidos
    var telefono: String=telefono
    // Siempre que se cree un objeto de este tipo, siempre se va a ejecutar.
    // ES solo un bloque de inicializacion, No es un Contructor
    init {
        println("nombres -->${nombres}")


    }
    //**************************
    //   Contructor SECUNDARIO
    //**************************

   /* constructor(idCliente: String,
                nombres: String,
                apellidos: String,
                telefono: String):this(nombres,apellidos,telefono){
        print("Testing")
    }*/
}