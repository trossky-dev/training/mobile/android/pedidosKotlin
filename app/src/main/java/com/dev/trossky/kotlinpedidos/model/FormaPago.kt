package com.dev.trossky.kotlinpedidos.model

/**
 * Created by luis on 3/10/17.
 */
class FormaPago (nombre: String) {
    var idFormaPago: String?=null
    var nombre: String=nombre

    constructor( idFormaPago: String, nombre: String): this(nombre){
        this.idFormaPago=idFormaPago
        this.nombre=nombre
    }

}