package com.dev.trossky.kotlinpedidos.model

/**
 * Created by luis on 3/10/17.
 */
class CabeceraPedido(fecha:String,
                     idCliente: String,
                     idFormaPago: String) {

    var idCabeceraPedido:String?=null
    var fecha:String=fecha
    var idCliente: String=idCliente
    var idFormaPago: String=idFormaPago

    constructor(idCabeceraPedido:String,
                fecha:String,
                idCliente: String,
                idFormaPago: String):this(fecha,idCliente,idFormaPago){
        this.idCabeceraPedido=idCabeceraPedido
        this.fecha=fecha
        this.idCliente=idCliente
        this.idFormaPago=idFormaPago

    }



}